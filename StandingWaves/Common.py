# SOME HELPFUL FUNCTIONS

import numpy as np
import scipy.fftpack as sfft

def FNoise(nSamples, alpha,sr):
    
    nu = sfft.rfftfreq(nSamples, d=1./sr)
    nu[0] = nu[1]/2.
    P = (1./np.abs(nu))**alpha    
    phases = np.cos(np.random.uniform(0,2*np.pi, size=nSamples))
    
    fp = np.random.normal( scale=np.sqrt(P))

    return sfft.irfft(fp)

def ArrayDot(a,b):
    """
    Perform a dot product on an array of M IxJ matrices.
    """
    _a = np.reshape(a, (a.shape[0], 1, a.shape[1], a.shape[2]))
    _a = np.transpose(_a, (2,1,0,3))
    _b = np.reshape(b, (1, b.shape[0], b.shape[1], b.shape[2]))

    return np.sum(_a*_b, axis=2)[::-1,::-1,:]

