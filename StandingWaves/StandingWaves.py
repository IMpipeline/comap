#################################
# STANDING WAVES TESTING MODULE #
#################################

import numpy as np
from matplotlib import pyplot
import Cables
import Optics
import Common

####################################################################################################################
# Some typical values for the two cables' slightly mismatched input and output, and the shunt capacitor in between #
####################################################################################################################

ZS=48.
ZL=50.
Z01=51.
Z02=52.
C=0.2 # pF


###########################
# Results for COMAP IF(?) #
###########################

nSamples = 2**8
sr = 32
t = np.linspace(0,nSamples/sr, nSamples)


chans = 1024
x = np.arange(chans)
y = np.zeros((chans, nSamples))

nulo = 24.
lowFreq = 26.
hiFreq = 34.
Tsys = 40.

nu = np.linspace(lowFreq, hiFreq, chans)
nuif = nu - nulo

# Temperature variations
dT = Common.FNoise(nSamples, 3., sr)
dT = (dT - np.mean(dT))/(np.max(dT) - np.min(dT))  * 15. + 22.


for i in range(nSamples):
    A1 = Cables.ABCDtl(Z01, Cables.d1(dT[i]), nuif, dT[i])
    A2 = Cables.ABCDsi(-1j/(2*np.pi*nuif*C*1e-3) )
    A3 = Cables.ABCDtl(Z02, Cables.d2(dT[i]), nuif, dT[i])

    Cables.ABCDj = Common.ArrayDot(A1,A2)
    Cables.ABCDi = Common.ArrayDot(Cables.ABCDj,A3)

    H = 2.*ZL/(Cables.ABCDi[0,0,:]*ZL+Cables.ABCDi[0,1,:]+Cables.ABCDi[1,0,:]*ZS*ZL+Cables.ABCDi[1,1,:]*ZS)
    y[:,i] = np.abs(H)**2


pmdl = np.poly1d(np.polyfit(nu, y[:,0], 2))

pyplot.figure(figsize=(12,8))
pyplot.plot(nu, y[:,0], '-')
pyplot.plot(nu, pmdl(nu), '-', linewidth=3, color='r')
pyplot.xticks(size=22)
pyplot.yticks(size=22)
pyplot.xlabel('Frequency (GHz)', size=25)
pyplot.ylabel('Gain', size=25)
pyplot.show()


