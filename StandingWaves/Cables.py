######################################################
# FUNCTIONS DESCRIBING CABLE MISMATCH STANDING WAVES #
######################################################

import numpy as np
import scipy.fftpack as sfft

#############################################
# Defining global value for temp. variation #
#############################################

delT=10

def d1(dT=delT,alCu=16e-06):
    return 1.5*(1+alCu*dT)

def d2(dT=delT,alCu=16e-06):
    return 180e-03*(1+alCu*dT)


########################################################################
# Empirical approximation for change in dielectric constant with temp. #
########################################################################

def ep(dT=delT):
    return 2.1*(1-1e-02*np.arctan(dT/0.5))

##################################
# Cable transmission ABCD matrix #
##################################

def ABCDtl(Z0,d,f,dT): # f in GHz
    
    y = 2j*np.pi*f*1e9*np.sqrt(ep(dT))/3e8
    
    A = np.cosh(y*d)
    B = Z0*np.sinh(y*d)
    C = 1/Z0*np.sinh(y*d)
    D = np.cosh(y*d)
    return np.array([[A,B],[C,D]])

###############################
# Shunt impedance ABCD matrix #
###############################

def ABCDsi(Z):
    if isinstance(Z, np.ndarray):
        A=np.ones(Z.size) + 1j*0.
        B=np.zeros(Z.size) + 1j*0.
        D=np.ones(Z.size) + 1j*0.
    else:
        A=1 + 1j*0.
        B=0 + 1j*0.
        D=1 + 1j*0.

        
    C=1./Z


    return np.array([[A,B],[C,D]])



