###############################################
# FUNCTIONS DESCRIBING OPTICAL STANDING WAVES #
###############################################

import numpy as np
import scipy.fftpack as sfft

#######################################################
# Model of reflection coefficient at secondary mirror #
#######################################################

def refsec(f,amp,fref,rphas,rlev,iphas,ilev):
    R = amp*np.cos(f/fref+rphas)+rlev
    I = amp*np.cos(f/fref+iphas)+ilev
    return R + 1j*I

#############################################################
# Model for phase delay between first and second reflection #
#############################################################

def beta(f,d,dT,alpha):
    # f in GHz, d:=distance of feed from secondary in m, 
    #     dT:=difference with average temperature, alpha:=expansion coefficent
    f = np.reshape(f, (f.size, 1))
    dT = np.reshape(dT, (1, dT.size))

    phi = 4.*np.pi*(f/c*1e9)*d*(1 + dT*alpha)
    return np.cos(phi) + 1j*np.sin(phi)

#############################################################
# Model for feed/waveguide/amplifier reflection coefficient #
#############################################################

def refwaveg(f,fmid,amp):
    return amp*fmid/f

